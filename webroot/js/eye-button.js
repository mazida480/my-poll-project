let eyeBtn = document.querySelector('.fa-eye') 
let password = document.querySelector('[type="password"]')

eyeBtn.addEventListener('mousedown', () =>
{
    password.attributes.type.value = 'text' ;
})

eyeBtn.addEventListener('mouseup', () =>
{
    password.attributes.type.value = 'password' ;
})