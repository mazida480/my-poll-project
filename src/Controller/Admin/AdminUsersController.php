<?php
    
    declare(strict_types=1) ;
    namespace App\Controller\Admin;

    use App\Controller\Admin\AppController ;
    use Cake\Datasource\ConnectionManager;
    use Cake\Http\ServerRequest;

    class AdminUsersController extends AppController 
    {
        public function initialize():void
        {
            parent::initialize() ;
            $this->loadModel('Polls') ;
            $this->loadModel('Options') ;
        }

        public function beforeFilter(\Cake\Event\EventInterface $event)
        {
            parent::beforeFilter($event);
            // Configure the login action to not require authentication, preventing
            // the infinite redirect loop issue
            $this->Authentication->addUnauthenticatedActions(['login', 'register']);
        }


        public function index()
        {
            return $this->redirect(['action' => 'option-table']) ;
        }

        public function redirectLogin()
        {
        }

        public function login()
        {
            $this->request->allowMethod(['get', 'post']);
            $result = $this->Authentication->getResult();
            // regardless of POST or GET, redirect if user is logged in
            if ($result && $result->isValid()) {
            // redirect to /articles after login success
            $redirect = $this->request->getQuery('redirect', [
            'prefix' => 'Admin',
            'controller' => 'AdminUsers',
            'action' => 'option-table',
            ]);
            return $this->redirect($redirect);
            }
            // display error if user submitted and authentication failed
            if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
            }
        }

        public function logout()
        {
            $result = $this->Authentication->getResult();
            // regardless of POST or GET, redirect if user is logged in
            if ($result && $result->isValid()) 
            {
            $this->Authentication->logout();
            return $this->redirect(['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'login']);
            }
        }


        public function register()
        {
            $admUser = $this->AdminUsers->newEmptyEntity() ;

            if($this->request->is('post'))
            {
                $admUser = $this->AdminUsers->patchEntity($admUser, $this->request->getData()) ;

                if($this->AdminUsers->save($admUser))
                {
                    $this->Flash->success(__('Admin is created'));
                    return $this->redirect(['actionn' => 'login']) ;

                }
                else
                {
                    $this->Flash->error(__("Couldn't create admin.")) ;
                }
            }

            $this->set(compact('admUser')) ;
        }

        public function optionTable()
        {
            $this->paginate = [
                'contain' => ['Polls'],
            ];
            $options = $this->paginate($this->Options, ['limit' => '4']);
    
            $this->set(compact('options'));
        }

        public function optionAdd()
        {
            $option = $this->Options->newEmptyEntity() ;

            if($this->request->is('post'))
            {
                $image =$this->request->getData('photo_url') ;
                $imageName = $image->getClientFilename() ;
                $imageType = $image->getClientMediaType() ;

                if($imageType == 'image/jpeg' || $imageType == 'image/jpg' || $imageType == 'image/png' )
                {

                    $targetPath = WWW_ROOT.'img'.DS.$imageName ;
                    $image->moveTo($targetPath);
                }
                
                

                $option = $this->Options->patchEntity($option, $this->request->getData()) ;

                $option->photo_url = "/img/".$imageName ;


                if($this->Options->save($option))
                {
                    $this->Flash->success("Options are added.") ;
                    return $this->redirect(['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'option-table']) ;
                }
                else
                {
                    $this->Flash->error("Options couldn't be added.Try again") ;
                }
            }

            
            $polls = $this->Options->Polls->find('list', ['limit' => 200])->all();
            $this->set(compact('option', 'polls'));
    
        }

        public function optionEdit($id=null)
        {
            $option = $this->Options->get($id, ['containes' => []]) ;

            if($this->request->is(['patch', 'put', 'post']))
            {
                $option = $this->Options->patchEntity($option, $this->Options->getData());

                if($this->Options->save($option))
                {
                    $this->Flash->success("Updated successfully.") ;
                    return $this->redirect(['action' => 'option-table']) ;
                }
                else
                {
                    $this->Flash->error("Couldn't update.Try again.") ;
                }
            }

            $polls = $this->Options->Polls->find('list', ['limit' => 200])->all();
            $this->set(compact('option', 'polls'));
    
        }

        public function optionView($id = null)
        {
            $option = $this->Options->get($id, [
                'contain' => ['Polls', 'Responses'],
            ]);
    
            $this->set(compact('option', ));
        }

        public function optionDelete($id=null)
        {
            $this->request->allowMethod(['post', 'delete']) ;
            $option = $this->Options->get($id) ;

            if($this->Options->delete($option))
            {
                $this->Flash->success(__('The option has been deleted.'));
                $connection = ConnectionManager::get('default');

                $connection->execute("ALTER TABLE options AUTO_INCREMENT = 1")  ;


            }
            else
            {
                $this->Flash->error(__('The option could not be deleted. Please, try again.'));

            }

            return $this->redirect(['action' => 'option-table']);

        }
    

        public function pollIndex()
        {
            $polls = $this->paginate($this->Polls);
            $this->set(compact('polls'));
        }

        public function pollView($id = null)
        {
            $poll = $this->Polls->get($id, [
                'contain' => ['Options'],
            ]);
    
            $this->set(compact('poll'));
        }

        public function pollAdd()
        {
            $poll = $this->Polls->newEmptyEntity();
            if ($this->request->is('post')) {
                $poll = $this->Polls->patchEntity($poll, $this->request->getData());
                if ($this->Polls->save($poll)) {
                    $this->Flash->success(__('The poll has been saved.'));
    
                    return $this->redirect(['action' => 'option-add']);
                }
                $this->Flash->error(__('The poll could not be saved. Please, try again.'));
            }
            $this->set(compact('poll'));
    
        }

        public function pollEdit($id = null)
        {
            $poll = $this->Polls->get($id, [
                'contain' => [],
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $poll = $this->Polls->patchEntity($poll, $this->request->getData());
                if ($this->Polls->save($poll)) {
                    $this->Flash->success(__('The poll has been saved.'));
    
                    return $this->redirect(['action' => 'poll-index']);
                }
                $this->Flash->error(__('The poll could not be saved. Please, try again.'));
            }
            $this->set(compact('poll'));
        }
    

        public function pollDelete($id=null)
        {
            
            $this->request->allowMethod(['post', 'delete']);
            $poll = $this->Polls->get($id);
            if ($this->Polls->delete($poll)) {
                $this->Flash->success(__('The poll has been deleted.'));
                $connection = ConnectionManager::get('default');
                $connection->execute("ALTER TABLE polls AUTO_INCREMENT = 1")  ;
            } 
            else 
            {
                $this->Flash->error(__('The poll could not be deleted. Please, try again.'));
            }
    
            return $this->redirect(['action' => 'poll-index']);
    
        }
    

        public function responseIndex()
        {
            $this->paginate = [
                'contain' => ['Options'],
            ];
            $responses = $this->paginate($this->Responses);
    
            $this->set(compact('responses'));
        }
    
    
    
    }

?>