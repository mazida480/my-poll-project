<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Poll helper
 */
class PollHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param int $responseCount
     *
     * @return string
     */
    public function result(int $responseCount): string
    {
        $total = $this->getView()->get('allResponses');
        $value = $responseCount * 100 / $total;

        return sprintf(

            '<div class="progress" role="progressbar" aria-valuenow="%d" aria-valuemin="0" aria-valuemax="100">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" value="%s" max="100" style="width: %d%%">%s %%</div>
            </div>',
            $value,
            $value,
            $value, 
            $value
        );
    }

}
?>
