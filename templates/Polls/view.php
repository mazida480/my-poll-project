<?php
    echo $this->Html->css('./poll-homepage/image.css') ;
?>
<section class="text-center bg-dark bg-gradient p-3 text-light mt-5">
    <div class="hero-body">
        <h2 class="title">
            <?= __('Poll')?>: <?= h($poll->name)?>
        </h2>
        <h3 class="subtitle">
            <?= __('Make your opinion count')?>
        </h3>
    </div>
</section>

<div class="box ">
    <?php foreach ($poll->options as $option):?>
    <article class="media  ">
        <figure class="text-center">
            <div class="image is-128x128  py-3 bg-dark bg-gradient ">

                <div class="img-container" data-aos="zoom-in-down">
                    <?= $this->Html->image($option->photo_url, [
                        'style' => 'height:90%', 'class' => 'card p-3 mx-auto '
                    ])?>
                </div>

            </div>
            <div class="media-content text-center bg-dark bg-gradient text-light p-3  border-5 border-bottom border-warning">
                <div class="content ">
                    <h4 class="">
                        <strong><?= h($option->name)?></strong>
                    </h4>
                </div>
                <?php if ($showResult):?>
                <?= $this->Poll->result($option->response_count)?>
                <?php else:?>
                <nav class="level is-mobile">
                    <div class="level-left">
                        <a class="level-item">
                            <div class="control">
    
                                <?= $this->Form->postButton(__('Vote'),
                                    [
                                        'action' => 'vote',
                                        $option->id,
                                    ],
                                    ['class' => ' btn btn-warning text-light fs-5']
                                )?>
                            </div>
                        </a>
                    </div>
                </nav>
                <?php endif;?>
            </div>
        </figure>
    </article>
    <?php endforeach;?>
</div>

