<?php
/**
 * @var \App\Model\Entity\Poll[] $polls
 * @var \App\View\AppView $this
 * @var bool $showResult
 */
?>
<section class="text-center bg-dark bg-gradient text-light my-3 p-3">
    <div class="hero-body">
        <h2 class="title">
            <?= __('All The Best Polls')?>
        </h2>
        <h3 class="subtitle">
            <?= __('The poll you looking for is here')?>
        </h3>
    </div>
</section>

<div class="box text-center bg-dark bg-gradient text-light p-3">
    <?php foreach ($polls as $poll):?>
    <article class="media">
        <figure class="media-left">
            <p class="image is-128x128">
                <?= $this->Html->image($poll->photo_url, [
                    'style' => 'height:90%',
                ])?>
            </p>
        </figure>
        <div class="media-content ">
            <div class="content ">
                <h3>
                    <strong><?= h($poll->name)?></strong>
                </h3>
            </div>
            <nav class="level is-mobile">
                <div class="level-left">
                    <a class="level-item">
                        <div class="control">
                            <button class="btn btn-warning">
                                <?= $this->Html->link(__('Vote Now'), ['action' => 'view', $poll->id], ['class' => 'link-light link-underline link-underline-opacity-0 fs-5']) ?>
                            </button>
                        </div>
                    </a>
                </div>
            </nav>
        </div>
    </article>
    <?php endforeach;?>
</div>

