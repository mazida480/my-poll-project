<?php
   echo $this->Html->css('./poll-homepage/home.css') ;
?>


    <div class="container p-0 bg-dark bg-gradient text-light position-absolute top-50 start-50 translate-middle  " data-aos="flip-left"
    data-aos="zoom-in-down">
        <div class="row ">
            <div class="col-xl col-md col-sm text-center">
            <div class="d-flex flex-row mb-3 bg-secondary p-1">
                <div id="c1" class="ms-1 rounded-circle border-dark "> </div>
                <div id="c2" class="ms-1 rounded-circle border-dark "> </div>
                <div id="c3" class="ms-1 rounded-circle border-dark "> </div>
            </div>
                <p id="cake">🍓</p>
                <h2 class="">Welcome To Our Poll Program</h2>
                <a href="Polls/main" class="display-3 "><i class="fa-solid fa-circle-right"></i></a>
                </h2>
            </div>
        </div>
    </div>
