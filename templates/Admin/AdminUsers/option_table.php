<?php
  $this->layout = 'admin' ;
  echo $this->element('../element/Admin/navbar');
?>
<div class="container-fluid   my-3">
  <div class="row p-2">
        <div class="col-md-3 bg-secondary">
      <h3 class="text-light"><?= __('Options') ?></h3>
      <ul>
        <li class="list-group-item d-block bg-info p-2 mt-2">
        <?= $this->Html->link(__('Add Poll'), ['action' => 'poll-add'], ['class' => 'link-light link-underline link-underline-opacity-0  d-block']) ?>
          </li>
      </ul>
    </div>
    <div class="col p-2 bg-info " >
      <table class="table table-striped table-dark">
        <thead class="text-center">
            <tr >
            <th><?= $this->Paginator->sort('id') ?></th>
              <th><?= $this->Paginator->sort('poll_name') ?></th>
              <th><?= $this->Paginator->sort('name') ?></th>
              <th><?= $this->Paginator->sort('photo_url') ?></th>
              <th><?= $this->Paginator->sort('response_count') ?></th>
              <th class="actions"><?= __('Actions') ?></th>

            </tr>
        </thead>
        <tbody class="text-center">
          <?php foreach($options as $opt):?>
            <tr >
              <td><?= $this->Number->format($opt->id) ?></td>
              <td><?= $opt->has('poll') ? $this->Html->link($opt->poll->name, ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'poll-index', $opt->poll->id]) : '' ?></td>

              <td><?= h($opt->name) ?></td>
              <td><?= h($opt->photo_url) ?></td>
              <td><?= h($opt->response_count) ?></td>
              <td class="actions">

                <?= $this->Html->link(__('View'), ['action' => 'option-view', $opt->id], ) ?>

                <?= $this->Html->link(__('Edit'), ['action' => 'option-edit', $opt->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['action' => 'option-delete', $opt->id], ['confirm' => __('Are you sure you want to delete # {0}?', $opt->id)]) ?>
                </td>
            </tr>

          <?php endforeach;?>
        </tbody>
      </table>

      <div class="paginator text-light text-end">
        <ul class="pagination  justify-content-end">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>

    </div>
  </div>
</div>