<?php
    $this->layout = 'admin' ;
    echo $this->Html->css('./poll-homepage/eye-button') ;

?>
<div class="container  position-absolute top-50 start-50 translate-middle">
    <div class="row justify-content-center">
         <div class="col-md-6 card p-5 shadow bg-body-tertiary rounded">
            <?= $this->Form->create($admUser) ?>
            <!-- <?= $this->Form->control(__('username'), ['class' => 'form-control'])?>
            <?= $this->Form->control(__('email'), ['class' => 'form-control'])?>
            <?= $this->Form->control(__('password'), ['class' => 'form-control'])?> --> 


            <div class="input-group mb-3">

                <span class="input-group-text w-25">Username</span>
                <?= $this->Form->input(__('username'), ['class' => 'form-control']) ?>
            </div>

            <div class="input-group mb-3">

                <span class="input-group-text w-25">Email</span>
                <?= $this->Form->input(__('email'), ['class' => 'form-control']) ?>
            </div>

            <div class="input-group mb-3">

                <span class="input-group-text w-25">Password</span>
                <?= $this->Form->input(__('password'), ['class' => 'form-control', 'type' => 'password']) ?>
                <span class="input-group-text"><i class="fa-solid fa-eye"></i></span>
            </div>

            <div class="text-center">
                <?= $this->Form->submit(__('Register'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?= $this->Form->end()?>
        </div>
    </div>
</div>
<?=  $this->Html->script('eye-button') ;?>