<?php
    echo $this->element('../element/Admin/navbar'); 
    $this->layout = 'admin' ;
?>
    <div class="container  my-3">
        <div class="row bg-light p-2">
            <div class="col-md-3 bg-secondary">
                <div class="side-nav">
                <h4 class="text-light"><?= __('Actions') ?></h4>
                <ul>
                    <li class="list-group-item mt-2">
                        <?= $this->Form->postLink(
                        __('Delete'),
                        ['action' => 'poll-delete', $poll->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $poll->id), 'class' => 'link-light bg-info p-2 d-block link-underline link-underline-opacity-0']) ?>
                    </li>

                    <li class="list-group-item mt-2">
                        <?= $this->Html->link(__('List Polls'), ['action' => 'poll-index'], ['class' => 'link-light bg-info p-2 d-block link-underline link-underline-opacity-0']) ?>
                    </li>
                </ul>

            </div>

            </div>
            <div class="col-md-9">
            <?= $this->Form->create($poll) ?>
            <fieldset>
                <legend><?= __('Edit Poll') ?></legend>
                <?php
                    echo $this->Form->control('name', ['class' => 'form-control']);
                    echo $this->Form->control('photo_url', ['class' => 'form-control', 'disabled']);
                ?>
            </fieldset>
            <div class="text-center mt-3">
                <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>

            </div>
        </div>
    </div>
