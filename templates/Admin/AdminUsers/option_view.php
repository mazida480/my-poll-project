<?php
  $this->layout = 'admin' ;
  echo $this->element('../element/Admin/navbar');
?>

<div class="container my-3">
    <div class="row">
        <div class="col-md-4  bg-secondary">
        <h4 class="text-light"><?= __('Actions') ?></h4>

             <ul class="">
                <li class="list-group-item bg-info p-2 mt-2">
                    <?= $this->Html->link(__('Edit Option'), ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'option-edit', $option->id], ['class' => 'side-nav-item link-light link-underline link-underline-opacity-0']) ?>
                </li>

                <li class="list-group-item bg-info p-2 mt-2">
                    <?= $this->Form->postLink(__('Delete Option'), ['action' => 'delete', $option->id], ['confirm' => __('Are you sure you want to delete # {0}?', $option->id), 'class' => 'side-nav-item link-light link-underline link-underline-opacity-0']) ?>
                </li>

                <li class="list-group-item bg-info p-2 mt-2">
                <?= $this->Html->link(__('List Options'), ['action' => 'option-table'], ['class' => 'side-nav-item link-light link-underline link-underline-opacity-0']) ?>
                </li>

                <li class="list-group-item bg-info p-2 mt-2">
                <?= $this->Html->link(__('New Option'), ['action' => 'option-add'], ['class' => 'side-nav-item link-light link-underline link-underline-opacity-0']) ?>
                </li>
             </ul>               

        </div>

        <div class="col bg-secondary">
        <h3 class="text-light"><?= h($option->name) ?></h3>
            <table class="table">
                <tr>
                    <th><?= __('Poll') ?></th>
                    <td><?= $option->has('poll') ? $this->Html->link($option->poll->name, ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'poll-view', $option->poll->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($option->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Photo Url') ?></th>
                    <td><?= h($option->photo_url) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($option->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Response Count') ?></th>
                    <td><?= $this->Number->format($option->response_count) ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col  bg-info">
            <h4 class="text-light">Related Responses</h4>
            <?php if (!empty($option->responses)) : ?>
            <table class="table">
                <thead>
                    <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Option Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($option->responses as $responses) : ?>
                    <tr>
                    <td><?= h($responses->id) ?></td>
                    <td><?= h($responses->option_id) ?></td>
                     <td><?= h($responses->created) ?></td>
                     <td class="actions">
                         <?= $this->Html->link(__('View'), ['controller' => 'Responses', 'action' => 'view', $responses->id]) ?>
                         <?= $this->Html->link(__('Edit'), ['controller' => 'Responses', 'action' => 'edit', $responses->id]) ?>
                         <?= $this->Form->postLink(__('Delete'), ['controller' => 'Responses', 'action' => 'delete', $responses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $responses->id)]) ?>
                        </td>
                        </tr>
                    <?php endforeach ;?>    
                </tbody>
            </table>
            <?php endif; ?>
        </div>
    </div>
</div>