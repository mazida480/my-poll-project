<?php
  echo $this->element('../element/Admin/navbar');
  $this->layout = 'admin' ;


?>
    <div class="container my-3">
        <div class="row">
            <div class="col-md-3 bg-secondary">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <p class="text-danger bg-light"><strong>Note<sup>*</sup>:</strong> Please add poll before adding the corresponding option.</p>

            <ul>
                <li class="list-group-item mt-2">
                <?= $this->Html->link(__('Add Poll'), ['action' => 'poll-add'], ['class' => 'link-light link-underline link-underline-opacity-0 bg-info d-block p-2']) ?>
            </li>
            <li class="list-group-item mt-2">
                <?= $this->Html->link(__('List Options'), ['action' => 'option-table'], ['class' => 'link-light link-underline link-underline-opacity-0 bg-info d-block p-2']) ?>
            </li>
            </ul>

            </div>
            <div class="col bg-info p-3">
            <?= $this->Form->create($option, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Add Option') ?></legend>
                <?php
                    echo $this->Form->control('poll_id',['class' => 'form-select'], ['options' => $polls]);

                    echo $this->Form->control('name', ['class'=>'form-control']);

                    echo $this->Form->control('photo_url',['type' => 'file'], ['class'=>'form-control']) ;


                    echo $this->Form->control('response_count', ['class'=>'form-control disabled', 'disabled']);
                ?>
            </fieldset>
            <div class="btn-container text-center mt-3">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>

            </div>
            <?= $this->Form->end() ?>

            </div>
        </div>
    </div>
