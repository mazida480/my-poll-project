<?php
$this->layout = 'admin' ;
echo $this->Html->css('./poll-homepage/eye-button') ;
echo $this->element('../element/Admin/navbar') ;
?>

<div class="container position-absolute top-50 start-50 translate-middle">
    <div class="row justify-content-center ">
        <div class="col-md-6 shadow p-5 mb-5 bg-body-tertiary rounded">
        <?= $this->Flash->render() ?>

            <?= $this->Form->create()?>
                <h3 class="text-center">Admin Login</h3>
            <div class="input-group mb-3">
                <span class="input-group-text w-25">Username</span>
                <input type="text" name="email" class="form-control" placeholder="Enter your email.">
            </div>

            <div class="input-group mb-3">
            <span class="input-group-text w-25">Password</span>
                <input type="password" class="form-control" name="password" aria-label="Amount (to the nearest dollar)">
                <span class="input-group-text"><i class="fa-solid fa-eye"></i></span>
            </div>

            <div class="text-center">
            <?= $this->Form->submit(__('Login'), ['class' => 'btn btn-primary'])?>

            </div>
            <?= $this->Form->end()?>

        </div>
    </div>
</div>
<?=  $this->Html->script('eye-button') ;?>