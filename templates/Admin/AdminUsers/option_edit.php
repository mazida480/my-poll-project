    <?= $this->element('../element/Admin/navbar'); ?>
    <?php $this->layout = 'admin' ;?>


    <div class="container my-3">
        <div class="row">
            <div class="col-md-3 bg-secondary">
            <h4 class="heading"><?= __('Actions') ?></h4>
                <ul>
                    <li class="list-group-item">
                        <?= $this->Form->postLink( __('Delete'),['action' => 'delete', $option->id],['confirm' => __('Are you sure you want to delete # {0}?', $option->id), 'class' => 'link-light link-underline link-underline-opacity-0 p-2 d-block bg-info mt-2'] ) ?>
                    </li>
                    <li class="list-group-item">
                        <?= $this->Html->link(__('List Options'), ['action' => 'option-table'], ['class' => 'link-light link-underline link-underline-opacity-0 p-2 d-block bg-info mt-2']) ?>
                    </li>
                </ul>
            </div>
            <div class="col bg-info p-3">
            <?= $this->Form->create($option) ?>
            <fieldset>
                <legend><?= __('Edit Option') ?></legend>
                <?php
                    echo $this->Form->control('poll_id',['class' => 'form-select'], ['options' => $polls]);
                    echo $this->Form->control('name', ['class' => 'form-control']);
                    echo $this->Form->control('photo_url', ['class' => 'form-control']);
                    echo $this->Form->control('response_count', ['class' => 'form-control','disabled'] );
                ?>
            </fieldset>
            <div class="text-center mt-3">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>

            </div>
            <?= $this->Form->end() ?>

            </div>
        </div>
    </div>
</div>
