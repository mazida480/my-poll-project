<?php
  echo $this->element('../element/Admin/navbar');
  $this->layout = 'admin' ;

?>
    <div class="container my-3">
        <div class="row bg-info">
            <div class="col-md-3 bg-secondary">
                <h4 class="text-light"><?= __('Actions') ?></h4>
                <ul class="">
                    <li class="list-group-item ">
                    <?= $this->Html->link(__('Edit Poll'), ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'poll-edit', $poll->id], ['class' => 'link-light link-underline link-underline-opacity-0 d-block mt-2 bg-info p-2']) ?>
                    </li>

                    
                    <li class="list-group-item ">
                    <?= $this->Form->postLink(__('Delete Poll'), ['action' => 'poll-delete', $poll->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poll->id), 'class' => 'link-light link-underline link-underline-opacity-0 d-block mt-2 bg-info p-2']) ?>

                    </li>
                    <li class="list-group-item ">
                    <?= $this->Html->link(__('List Polls'), ['action' => 'poll-index'], ['class' => 'link-light link-underline link-underline-opacity-0 d-block mt-2 bg-info p-2']) ?>
                    </li>

                    <li class="list-group-item ">
                    <?= $this->Html->link(__('New Poll'), ['action' => 'poll-add'], ['class' => 'link-light link-underline link-underline-opacity-0 d-block mt-2 bg-info p-2']) ?>
                    </li>
                </ul>


            </div>
            <div class="col-md-9">
                <h3 class="text-light"><?= h($poll->name) ?></h3>
                <table class="table">
                    <tr>
                        <th><?= __('Name') ?></th>
                        <td><?= h($poll->name) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Photo Url') ?></th>
                        <td><?= h($poll->photo_url) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Id') ?></th>
                        <td><?= $this->Number->format($poll->id) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Created') ?></th>
                        <td><?= h($poll->created) ?></td>
                    </tr>
                </table>
            </div>

        <div class="row">
            <div class="col">
            <div class="text-light">
            <h4><?= __('Related Options') ?></h4>
            <?php if (!empty($poll->options)) : ?>
            <div >
                <table class="table table-striped">
                    <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Poll Id') ?></th>
                        <th><?= __('Name') ?></th>
                        <th><?= __('Photo Url') ?></th>
                        <th><?= __('Response Count') ?></th>
                    </tr>
                    <?php foreach ($poll->options as $options) : ?>
                    <tr>
                        <td><?= h($options->id) ?></td>
                        <td><?= h($options->poll_id) ?></td>
                        <td><?= h($options->name) ?></td>
                        <td><?= h($options->photo_url) ?></td>
                        <td><?= h($options->response_count) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php endif; ?>
        </div>

            </div>
        </div>
        </div>
    </div>
</div>
