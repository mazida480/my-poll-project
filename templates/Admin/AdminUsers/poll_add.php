<?php
  echo $this->element('../element/Admin/navbar');
  $this->layout = 'admin' ;


?>
        <div class="container position-absolute top-50 start-50 translate-middle ">
            <div class="row  ">
                <div class="col-md-3 bg-secondary p-3">
                    <h4 class="text-light"><?= __('Actions') ?></h4>
                    <ul>
                        <li class="list-group-item ">
                        <?= $this->Html->link(__('List Polls'), ['action' => 'poll-index'], ['class' => 'bg-info p-2 d-block link-light link-underline link-underline-opacity-0']) ?>

                        </li>
                    </ul>
                </div>
            <div class="col p-3 bg-info">
            <?= $this->Form->create($poll) ?>
            <fieldset>
                <legend><?= __('Add Poll') ?></legend>
                <?php
                    echo $this->Form->control('name', ['class' => 'form-control']);
                    echo $this->Form->control('photo_url', ['class' => 'form-control', 'disabled' => '1', 'placeholder' => 'photo_url is associted with options table'] );
                ?>
            </fieldset>
            <div class="text-center mt-3">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>

            </div>
            <?= $this->Form->end() ?>

            </div>
        </div>
    </div>
</div>
