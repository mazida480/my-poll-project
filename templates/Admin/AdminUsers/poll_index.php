<?php
  echo $this->element('../element/Admin/navbar');
  $this->layout = 'admin' ;

 ?>
<div class="container  bg-info  my-3">
    <div class="row">
        <div class="col-md-3 bg-secondary">
        <h3 class="text-light"><?= __('Polls') ?></h3>
        <ul>
            <li class="list-group-item mt-2">
            <?= $this->Html->link(__('New Poll'), ['action' => 'poll-add'], ['class' => 'link-light bg-info p-2 d-block']) ?>
            </li>

            <li class="list-group-item mt-2">
            <?= $this->Html->link(__('List Options'), ['action' => 'option-table'], ['class' => 'link-light bg-info p-2 d-block']) ?>
            </li>
        </ul>
        </div>

        <div class="col-md-9 p-0 ">
            <table class="table table-striped table-dark table-responsive">
                <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('id') ?></th>
                        <th><?= $this->Paginator->sort('name') ?></th>
                        <th><?= $this->Paginator->sort('photo_url') ?></th>
                        <th><?= $this->Paginator->sort('created') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($polls as $poll): ?>
                    <tr>
                        <td><?= $this->Number->format($poll->id) ?></td>
                        <td><?= h($poll->name) ?></td>
                        <td><?= h($poll->photo_url) ?></td>
                        <td><?= h($poll->created) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'pollView', $poll->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $poll->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'poll-delete', $poll->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poll->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination justify-content-end">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-end"><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
