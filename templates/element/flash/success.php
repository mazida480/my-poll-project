<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="border border-1 border-success rounded bg-success-subtle text-success text-center" onclick="this.classList.add('hidden')"><?= $message ?></div>
