<?php
   echo $this->Html->css('./poll-homepage/home.css') ;
?>

<div class="d-flex flex-row-reverse bg-body-tertiary position-sticky top-0 z-3">

  <div class="p-2">
    <?php if($this->Identity->isLoggedIn()):?>
      <span><strong><?= $this->Identity->get('username');?></strong></span>
      <button class="btn btn-primary">
      <?= $this->Html->link(__('Logout'), ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'register'], ['class' => 'link-underline link-underline-opacity-0 link-light'])?>
    </button>
    <?php else: ?>
        <button class="btn btn-primary">
        <?= $this->Html->link(__('Sign Up'), ['prefix' => 'Admin', 'controller' => 'AdminUsers', 'action' => 'register'], ['class' => 'link-underline link-underline-opacity-0 link-light'])?>
      </button>
    <?php endif;?>

</div>
</div>


